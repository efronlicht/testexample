package subpackage

import "testing"

func TestSubpackage(t *testing.T) {
	t.Logf("example of t.Logf")
}

func TestSkipOnShort(t *testing.T) {
	if testing.Short() {
		t.Skipf("skip %s: short mode", t.Name())
	}
}
