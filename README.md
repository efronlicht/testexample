# Go Test Cheatsheet

This README and repository are meant to help you get a quick and dirty understanding of how to write 
decent tests in Go. 

## How to use this repo:
- Read the README
- Explore the repository's source code for illustrated examples and commentary on the tests in practice.

## Table of Contents
- [Go Tests](#go-tests)
  - [Table of Contents](#table-of-contents)
  - [Running Go Tests](#running-go-tests)
    - [Flags](#flags)
    - [example - no flags](#example---no-flags)
    - [example: all common flags](#example-all-common-flags)
  - [Packages and Files](#packages-and-files)
    - [Table: Test File and Function Naming](#table-test-file-and-function-naming)
  - [Core API](#core-api)
    - [Table: Testing Method and Use](#table-testing-method-and-use)
  - [The core of testing: "want X, got Y"](#the-core-of-testing-want-x-got-y)
    - [Example Function: TitleCase](#example-function-titlecase)
    - [Technique: Table-Driven Tests](#technique-table-driven-tests)
    - [Designing functions for table-driven tests](#designing-functions-for-table-driven-tests)
    - [Testing Techniques](#testing-techniques)
      - [Errors](#errors)
        - [Example: TestBase32Decode](#example-testbase32decode)
      - [panics](#panics)

## Running Go Tests

Normal usage: `go test ./...` with optional flags.

```sh
go test [-v] [-short] [-cover] path/to/package ./... # test a package and all subpackages
```
### Flags
- `-v` verbose output: print the name of each test as it runs, all log messages, and the result of each test.
- `-short` short mode: skip tests that take a long time to run (as defined by the test itself).
- `-cover` coverage: print the percentage of statements that were executed during the test, per-package.


### example - no flags
```sh
go test ./...
```
```
ok      gitlab.com/efronlicht/testexample       0.001s
ok      gitlab.com/efronlicht/testexample/subpackage    0.001s
```

### example: all common flags
```sh
go test -v -short -cover ./...
```

```
=== RUN   TestAdd
--- PASS: TestAdd (0.00s)
=== RUN   TestTitleCaseKey
--- PASS: TestTitleCaseKey (0.00s)
=== RUN   TestMulIntegration
--- PASS: TestMulIntegration (0.00s)
PASS
coverage: 76.0% of statements
ok  	gitlab.com/efronlicht/testexample	(cached)	coverage: 76.0% of statements
=== RUN   TestSubpackage
    subpackage_test.go:6: example of t.Logf
--- PASS: TestSubpackage (0.00s)
=== RUN   TestSkipOnShort
    subpackage_test.go:11: skip TestSkipOnShort: short mode
--- SKIP: TestSkipOnShort (0.00s)
PASS
coverage: [no statements]
ok  	gitlab.com/efronlicht/testexample/subpackage	(cached)	coverage: [no statements]
```

## Packages and Files
- Files ending with _test.go are considered test files.
- Functions in test files with the signature `func TestXxx(*testing.T)` are considered tests.   
- Test files must have either the same package name as the package being tested (for "unit") tests or the package name suffixed with _test (for "integration" tests)

### Table: Test File and Function Naming
The following table assumes a package called `example`.

| File | Package | Function | OK | Comment |
| --- | --- | --- | --- | --- |
| _test.go | `example` | TestMul(t *testing.T) | ✅ | minimum requirement |
| testexample.go | `example` | TestMul(t *testing.T) | X | test files must end with _test.go |
| unit_test.go | `example` | TestAdd(t *testing.T) | ✅ | - |
| unit_test.go | `example` | Testadd(t *testing.T) | X | test functions must start with a capital letter |
| integration_test.go | `example_test` | TestAdd(t *testing.T) | ✅ | - |
| integration_test.go | `exampletest` | TestAdd(t *testing.T) | X | test package must either be the same as the package being tested or suffixed with _test |


## Core API
The following core API is all you need to write tests in Go:

### Table: Testing Method and Use 
| Method | Use | Example |
| --- | --- | --- |
| `(*T).Logf(string, ..any)` | Log a message |
| `(*T).Name()` | Get the name of the test | - |
| `(*T).Errorf(string, ..any)` | Mark test as failed with message and continue | `t.Errorf("expected %d, got %d", 1, 2)` |
| `(*T).Fatalf(string, ..any)` | Mark test as failed with message and stop | `t.Fatalf("expected %d, got %d", 1, 2)` |
| `(*T).Skipf(string)` | Skip test with message | `t.Skipf("skip %s: touches the network", t.Name())` |
| `(*T).Short()` | Return `true` if -short flag is set | `if t.Short() { t.Skip("skipping test in short mode") }` |


## The core of testing: "want X, got Y"

Tests are _easiest to understand_ and _least fragile_ when they operate on discrete inputs and outputs. A test that can be easily expressed in the form "want X, got Y" is usually a good test.

The following example function, `TitleCase`, was taken from my [series of articles on backend web development in go, "Backend Basics"](https://eblog.fly.dev/backendbasics.html)


### Example Function: TitleCase
Given just the function signature `TitleCaseKey(key string) string`, we can write a test that checks the function's behavior for a specific input.
```go
func TestTitleCaseKey(t *testing.T) {
    got := TitleCaseKey("hello")
    want := "Hello"
    if got != want {
        t.Errorf("want %q, got %q", want, got)
    }

    // or more concisely:
    if got, want := TitleCaseKey("hello"), "Hello"; got != want {
        t.Errorf("want %q, got %q", want, got)
    }
}
```

### Technique: Table-Driven Tests
You can define a **test struct** that contains the input and expected output for a test case. Then, you can iterate over a slice of test cases to run the tests. Let's take another shot at testing `TitleCaseKey` using this technique.

```go
func TestTitleCaseKey(t *testing.T) {
	type TestCase struct{ input, want string }
	testCases := []TestCase{
		{"foo-bar", "Foo-Bar"},
		{"cONTEnt-tYPE", "Content-Type"},
		{"host", "Host"},
		{"host-", "Host-"},
		{"ha22-o3st", "Ha22-O3st"},
	}
	for _, tt := range testCases {
		if got := AsTitle(tt.input); got != tt.want {
			t.Errorf("want %q, got %q", tt.want, got)
		}
	}
}
```

This can be expressed more concisely by using anonymous structs:

```go
func TestTitleCaseKey(t *testing.T) {
    testCases := []struct{ input, want string }{
        {"foo-bar", "Foo-Bar"},
        {"cONTEnt-tYPE", "Content-Type"},
        {"host", "Host"},
        {"host-", "Host-"},
        {"ha22-o3st", "Ha22-O3st"},
    }
    for _, tt := range testCases {
        if got := AsTitle(tt.input); got != tt.want {
            t.Errorf("want %q, got %q", tt.want, got)
        }
    }
}
```
And _even more concisely_ by defining the test cases inline:

```go
func TestTitleCaseKey(t *testing.T) {
    for _, tt := range []struct{ input, want string }{
        {"foo-bar", "Foo-Bar"},
        {"cONTEnt-tYPE", "Content-Type"},
        {"host", "Host"},
        {"host-", "Host-"},
        {"ha22-o3st", "Ha22-O3st"},
    } {
        if got := AsTitle(tt.input); got != tt.want {
            t.Errorf("want %q, got %q", tt.want, got)
        }
    }
}
```

This final form is the "idiomatic" way to write table-driven tests in Go, but all three forms are exactly equivlent. Pick the one that you find most readable.

### Designing functions for table-driven tests
- Pass in only what you need.
- Write _functions_ (where possible) rather than _methods_.
- `return` changes rather than _mutating_ state.
- `return` errors rather than panicking or logging: let the caller decide what to do with the error.
- pass in everything the function needs to do its job (http clients, databases, state, etc.)
- pass in "fully cooked" data: give the result of an environment variable lookup, not the environment variable itself.

BAD:
```go
type Pod struct {
    Name string
    Namespace string
    Labels map[string]string
    Container struct {
        //many fields omitted 
    }
}   

func (p *Pod) Canonicalize() {
    p.Name = strings.ReplaceAll(p.Name, "_", "-")
    p.Name = strings.ReplaceAll(p.Name, "-", "-")
    p.Name = AsTitle(p.Name)
}
```

Why is this bad?
- Not obvious what parts of the struct need to be set for the function to work.
- Function modifies the struct

GOOD:
```go
func canonicalizePodName(name string) string {
    name = strings.ReplaceAll(name, "_", "-")
    name = strings.ReplaceAll(name, "-", "-")
    return AsTitle(name)
}
```

### Testing Techniques 

#### Errors
 test _that an error occurred_ rather than for _specific error messages_.

##### Example: TestBase32Decode

```go
func TestBase32Decode(t *testing.T) {
	type TestCase struct {
		name    string
		input   string
		want    string
		wantErr bool
	}
	testCases := []TestCase{
		{name: "empty string"},
		{name: "invalid input", input: "1", wantErr: true},
		{name: "valid input", input: "GEZDGNBVGY3TQOJQ", want: "1234567890"},
	}
	for _, tt := range testCases {
		got, err := base32.StdEncoding.DecodeString(tt.input)
		if err != nil && !tt.wantErr {
			t.Errorf("case %s: unexpected error: %v", tt.name, err)
		}
		if tt.wantErr && err == nil {
			t.Errorf("case %s: expected error, got nil", tt.name)
		}
		if string(got) != tt.want {
			t.Errorf("case %s: want %q, got %q", tt.name, tt.want, got)
		}
	}
}
```
#### panics
Panics fail a test. Use go's ordinary panic and recover mechanism to test that a function panics when it should.

```go
func sqrt(x float64) float64 {
    if x <= 0 {
        panic("sqrt of non-positive number %v", x)
    }
    return math.Sqrt(x)
}

func sqrt(x float64) float64 {
	if x <= 0 {
		panic(fmt.Errorf("sqrt of non-positive number %v", x))
	}
	return math.Sqrt(x)
}

func TestPanicOnNegative(t *testing.T) {
    // defer() executes in function scope, so we define a new function to capture the panic
    // and surface it.
    // neat go trick: Named Returns let you modify the return value of a function in a defer clause.

    // checkedSqrt returns the result of sqrt and whether it panicked.
    // result != 0 iff !panicked
	checkedSqrt := func(x float64) (result float64, panicked bool) {
		defer func() {
			if r := recover(); r != nil {
				panicked = true
			}
		}()
		result = sqrt(x)
		return result, panicked
	}
	for _, tt := range []struct {
		input     float64
		want      float64
		wantPanic bool
	}{
		{input: 1, want: 1},
		{input: 0, wantPanic: true},
		{input: -1, wantPanic: true},
	} {
		if got, panicked := checkedSqrt(tt.input); panicked != tt.wantPanic {
			t.Errorf("want panic=%t, got panic=%t", tt.wantPanic, panicked)
		} else if got != tt.want {
			t.Errorf("want %f, got %f", tt.want, got)
		}
	}
}
```
