// if you want to test the INSIDE API of your package, like a developer would,
// make a file in the package with _test in the name.
// this file is part of the package.
package testexample

import (
	"encoding/base32"
	"fmt"
	"math"
	"testing"
)

func TestAdd(t *testing.T) {
	const want = 4
	if got := add(2, 2); got != want {
		t.Errorf("add(2, 2) = %d; want %d", got, want)
	}
}

func TestBase32Decode(t *testing.T) {
	type TestCase struct {
		name    string
		input   string
		want    string
		wantErr bool
	}
	testCases := []TestCase{
		{name: "empty string"},
		{name: "invalid input", input: "1", wantErr: true},
		{name: "valid input", input: "GEZDGNBVGY3TQOJQ", want: "1234567890"},
	}
	for _, tt := range testCases {
		got, err := base32.StdEncoding.DecodeString(tt.input)
		if err != nil && !tt.wantErr {
			t.Errorf("case %s: unexpected error: %v", tt.name, err)
		}
		if tt.wantErr && err == nil {
			t.Errorf("case %s: expected error, got nil", tt.name)
		}
		if string(got) != tt.want {
			t.Errorf("case %s: want %q, got %q", tt.name, tt.want, got)
		}
	}
}

func TestTitleCaseKey(t *testing.T) {
	type TestCase struct{ input, want string }
	testCases := []TestCase{
		{"foo-bar", "Foo-Bar"},
		{"cONTEnt-tYPE", "Content-Type"},
		{"host", "Host"},
		{"host-", "Host-"},
		{"ha22-o3st", "Ha22-O3st"},
	}
	for _, tt := range testCases {
		if got := AsTitle(tt.input); got != tt.want {
			t.Errorf("want %q, got %q", tt.want, got)
		}
	}
}

func sqrt(x float64) float64 {
	if x <= 0 {
		panic(fmt.Errorf("sqrt of non-positive number %v", x))
	}
	return math.Sqrt(x)
}

func TestPanicOnNegative(t *testing.T) {
	checkedSqrt := func(x float64) (result float64, panicked bool) {
		defer func() {
			if r := recover(); r != nil {
				panicked = true
			}
		}()
		result = sqrt(x)
		return result, panicked
	}
	for _, tt := range []struct {
		input     float64
		want      float64
		wantPanic bool
	}{
		{input: 1, want: 1},
		{input: 0, wantPanic: true},
		{input: -1, wantPanic: true},
	} {
		if got, panicked := checkedSqrt(tt.input); panicked != tt.wantPanic {
			t.Errorf("want panic=%t, got panic=%t", tt.wantPanic, panicked)
		} else if got != tt.want {
			t.Errorf("want %f, got %f", tt.want, got)
		}
	}
}
