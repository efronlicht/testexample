// Package testexample serves as an example of go testing.
//
// # Test Basics
//
// - Files ending with _test.go are considered test files.
// - Functions in test files with the signature `func TestXxx(*testing.T)` are considered tests.
//
// # Valid Tests
//
// unit_test.go/TestAdd(t *testing.T): OK
// testexample.go/TestMul(t *testing.T): No: test files must end with _test.go
// _test.go/TestMul(t *testing.T): OK (but not recommended: give it a meaningful name!)
// unit_test.go/Testadd(t *testing.T): No: test functions must start with a capital letter

// ## Unit Tests
package testexample

import (
	"strings"
)

// go reminder: items in global scope with a capital letter are exported, items with a lowercase letter are not

// unexported function
func add(a, b int) int { return a + b }

// exported function
func Mul(a, b int) int { return a * b }

// newTitleCase returns the given header key as title case; e.g. "content-type" -> "Content-Type";
// it always allocates a new string.
func newTitleCase(key string) string {
	var b strings.Builder
	b.Grow(len(key))
	for i := range key {
		// the old c-style trick of using ASCII math to convert between upper and lower case can be
		// a bit confusing at first, but it's a nice trick to have in your toolbox.
		// the idea is as follows: A..=Z are 65..=90, and a..=z are 97..=122.
		// so upper-case letters are 32 less than their lower-case counterparts (or 'a'-'A' == 32).
		// rather than using the 'magic' number 32, we use 'a'-'A' to make it (somewhat) more clear what's going on.
		if i == 0 || key[i-1] == '-' {
			if key[i] >= 'a' && key[i] <= 'z' {
				b.WriteByte(key[i] + 'A' - 'a')
			} else {
				b.WriteByte(key[i])
			}
		} else if key[i] >= 'A' && key[i] <= 'Z' {
			b.WriteByte(key[i] + 'a' - 'A')
		} else {
			b.WriteByte(key[i])
		}
	}
	return b.String()
}

// isTitleCase returns true if the given header key is already title case; i.e, it is of the form "Content-Type" or "Content-Length", "Some-Odd-Header", etc.
func isTitleCase(key string) bool {
	// check if this is already title case.
	for i := range key {
		if i == 0 || key[i-1] == '-' {
			if key[i] >= 'a' && key[i] <= 'z' {
				return false
			}
		} else if key[i] >= 'A' && key[i] <= 'Z' {
			return false
		}
	}
	return true
}

// AsTitle returns the given header key as title case; e.g. "content-type" -> "Content-Type"
func AsTitle(key string) string {
	if isTitleCase(key) {
		return key
	}
	return newTitleCase(key)
}
