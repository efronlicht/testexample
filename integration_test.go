// If you want to test the OUTSIDE API of your package, like a consumer would,
// add _test to the package name.
// You can then import the package as normal.
// These files are NOT part of the package and won't be compiled at all unless using `go test`.
package testexample_test

import (
	"testing"

	"gitlab.com/efronlicht/testexample"
)

func TestMulIntegration(t *testing.T) {
	const want = 4
	if got := testexample.Mul(2, 2); got != want {
		t.Errorf("Mul(2, 2) = %d; want %d", got, want)
	}
}
